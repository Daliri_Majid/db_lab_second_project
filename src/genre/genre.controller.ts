import { Body, Controller, Get, Post, Delete, Put, Query } from '@nestjs/common';
import GenreServices from './genre.service';
import CreateGenreDto from './dto/create-genre.dto';
import UpdateGenreDto from './dto/update-genre.dto';
import {ApiResponse, ApiBearerAuth, ApiQuery } from '@nestjs/swagger';

@Controller('genre')
export default class GenreController {
    constructor(private readonly genreServices: GenreServices) {}

    @ApiResponse({ status: 200, description: "" })
    @ApiBearerAuth()
    @Post('addgenre')
    postGenre( @Body() genre: CreateGenreDto) {
        return this.genreServices.add(genre);
    }

    @ApiResponse({ status: 200, description: "" }) 
    @ApiBearerAuth()
    @Get()
    getAll() {
        return this.genreServices.getAll();
    }

    @ApiResponse({ status: 200, description: "" })
    @ApiQuery({ name: 'genre_id', required: true, type: Number, description: ``})
    @ApiBearerAuth()
    @Delete('delete')
    deleteGenre(@Query('genre_id') genreID) {
        return this.genreServices.delete(genreID);
    }

    @ApiResponse({ status: 200, description: "" })
    @ApiBearerAuth()
    @Put('update')
    updateGenre(@Body() genre: UpdateGenreDto) {
        return this.genreServices.update(genre);
        }
}