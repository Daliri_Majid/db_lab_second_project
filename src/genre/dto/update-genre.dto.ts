import { Length , IsOptional, Min, IsNumber } from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export default class UpdateGenreDto {

    @ApiProperty({description:"Id : ", type: "number"})
    readonly id: number;

    @ApiProperty({description:"Type : ", type: "string"})
    readonly type: string;
  }