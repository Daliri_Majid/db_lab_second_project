import { Length , IsOptional, Min, IsNumber } from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export default class CreateGenreDto {
  @ApiProperty({description:"Type : ", type: "string"})
    readonly type: string;
  }