import { Body, Controller, Get, ParseIntPipe, Post, Put, Header, Delete, Query } from '@nestjs/common';
import { Public } from '../public.decorator';
import {ApiResponse, ApiBearerAuth, ApiQuery } from '@nestjs/swagger';
import { JobseekersService } from './jobseekers.service';
import CreateEmployerDto from './dto/create-employer.dto';
import CreateFreelancerDto from './dto/create-freelancer.dto';
import CreateProjectDto from './dto/create-project.dto';


@Controller('jobseekers')
export class JobseekersController {
    constructor(private readonly jobseekersServices: JobseekersService) {}

    @Header('Content-Type', 'application/json')
    @ApiResponse({ status: 200, description: "" })
    @ApiBearerAuth()
    @Post('employer/new')
    addEmployer( @Body() employer: CreateEmployerDto) {
        return this.jobseekersServices.addEployer(employer);
    }
    @ApiResponse({ status: 200, description: "" })
    @ApiBearerAuth()
    @Get('employer/all')
    getAllEmpolyers() {
        return this.jobseekersServices.getAllEmployers();
    }
    @ApiResponse({ status: 200, description: "" })  
    @ApiBearerAuth()
    @Get('employer/projects')
    getProjectsOfEmpolyer( @Body('employerId', ParseIntPipe) employerId: number ) {
        return this.jobseekersServices.getProjectsOfEmployer(employerId);
    }

    @Header('Content-Type', 'application/json')
    @ApiResponse({ status: 200, description: "" })
    @ApiBearerAuth()
    @Post('freelancer/new')
    addFreelancer( @Body() freelancer: CreateFreelancerDto) {
        return this.jobseekersServices.addFreelancer(freelancer);
    }
    @ApiResponse({ status: 200, description: "" })
    @ApiBearerAuth()
    @Get('freelancer/all')
    getAllFreelancers() {
        return this.jobseekersServices.getAllFreelancers();
    }
    @ApiResponse({ status: 200, description: "" })
    @ApiBearerAuth()
    @Post('freelancer/accept')
    acceptProject( @Query('project_id', ParseIntPipe) ProjectID, @Query('freelancer_id', ParseIntPipe) FreelancerID) {
        return this.jobseekersServices.acceptProject(FreelancerID, ProjectID);
    }
    @ApiResponse({ status: 200, description: "" })
    @ApiBearerAuth()
    @Post('freelancer/apply')
    applyProject( @Query('project_id', ParseIntPipe) ProjectID, @Query('freelancer_id', ParseIntPipe) FreelancerID) {
        return this.jobseekersServices.applyProject(FreelancerID, ProjectID);
    }
    @ApiResponse({ status: 200, description: "" })  
    @ApiBearerAuth()
    @Post('freelancer/accepted_projects')
    getAcceptedProjectsfree( @Body('freelancerId', ParseIntPipe) freelancerId: number ) {
        return this.jobseekersServices.getAcceptedProjectsOfFreelancer(freelancerId);
    }
    @ApiResponse({ status: 200, description: "" })  
    @ApiBearerAuth()
    @Get('freelancer/applied_projects')
    getAppliedProjectsfree( @Body('freelancerId', ParseIntPipe) freelancerId: number ) {
        return this.jobseekersServices.getAppliedProjectsOfFreelancer(freelancerId);
    }
    
    @Header('Content-Type', 'application/json')
    @ApiResponse({ status: 200, description: "" })
    @ApiBearerAuth()
    @Post('project/new')
    addProject( @Body() project: CreateProjectDto) {
        return this.jobseekersServices.addProject(project);
    }
    @Header('Content-Type', 'application/json')
    @ApiResponse({ status: 200, description: "" })
    @ApiBearerAuth()
    @Post('project/delete')
    deleteProject(@Query('ProjectID') ProjectID) {
        return this.jobseekersServices.deleteProject(ProjectID);
    }
}
