import { Injectable } from '@nestjs/common';
import CreateEmployerDto from './dto/create-employer.dto';
import CreateFreelancerDto from './dto/create-freelancer.dto';
import EmployerEntity from 'src/db/employer.entity';
import FreelancerEntity from 'src/db/freelancer.entity';
import ProjectEntity from 'src/db/project.entity';
import CreateProjectDto from './dto/create-project.dto'

@Injectable()
export class JobseekersService {
    async addEployer(employerDetails: CreateEmployerDto): Promise<EmployerEntity> {
        const employerEntity: EmployerEntity = EmployerEntity.create();
        const { first_name, last_name, username, password, email, phone_number, address} = employerDetails;
        employerEntity.first_name = first_name;
        employerEntity.last_name = last_name;
        employerEntity.username = username;
        employerEntity.password = password;
        employerEntity.email = email;
        employerEntity.phone_number = phone_number;
        employerEntity.address = address;
        await EmployerEntity.save(employerEntity);
        return employerEntity;
    }
    async getProjectsOfEmployer(employerID: number): Promise<ProjectEntity[]> {
        const employer: EmployerEntity = await EmployerEntity.findOne({where: {id: employerID}, relations: ['projects']});
        return employer.projects;
    }
    async getAllEmployers(): Promise<EmployerEntity[]> {
        return await EmployerEntity.find();
    }

    async addFreelancer(employerDetails: CreateFreelancerDto): Promise<FreelancerEntity> {
        const freelancerEntity: FreelancerEntity = FreelancerEntity.create();
        const { first_name, last_name, username, password, email, phone_number, address, score} = employerDetails;
        freelancerEntity.first_name = first_name;
        freelancerEntity.last_name = last_name;
        freelancerEntity.username = username;
        freelancerEntity.password = password;
        freelancerEntity.email = email;
        freelancerEntity.phone_number = phone_number;
        freelancerEntity.address = address;
        freelancerEntity.score = score;
        await FreelancerEntity.save(freelancerEntity);
        return freelancerEntity;
    }
    async acceptProject(projectID: number, freelancerID : number): Promise<FreelancerEntity> {
        const freelancer = await FreelancerEntity.findOne(freelancerID);
        const project = await ProjectEntity.findOne(projectID);
        if(freelancer != undefined && project != undefined){
            if(freelancer.accepted_projects == undefined){
                freelancer.accepted_projects = [];
            }
            freelancer.accepted_projects.push(project);
            freelancer.save();
        }
        return freelancer;
    }
    async applyProject(projectID: number, freelancerID : number): Promise<FreelancerEntity> {
        const freelancer = await FreelancerEntity.findOne(freelancerID);
        const project = await ProjectEntity.findOne(projectID);
        if(freelancer != undefined && project != undefined){
            if(freelancer.applied_projects == undefined){
                freelancer.applied_projects = [];
            }
            freelancer.applied_projects.push(project);
            freelancer.save();
        }
        return freelancer;
    }
    async getAcceptedProjectsOfFreelancer(freelancerID: number): Promise<ProjectEntity[]> {
        const freelancer: FreelancerEntity = await FreelancerEntity.findOne({where: {id: freelancerID}, relations: ['accepted_projects']});
        return freelancer.accepted_projects;
    }
    async getAppliedProjectsOfFreelancer(freelancerID: number): Promise<ProjectEntity[]> {
        const freelancer: FreelancerEntity = await FreelancerEntity.findOne({where: {id: freelancerID}, relations: ['applied_projects']});
        return freelancer.applied_projects;
    }
    async getAllFreelancers(): Promise<FreelancerEntity[]> {
        return await FreelancerEntity.find();
    }

    async addProject(projectDetails: CreateProjectDto): Promise<ProjectEntity> {
        const projectEntity: ProjectEntity = ProjectEntity.create();
        const { title, subject, priority, size, description, required_skills } = projectDetails;
        projectEntity.title = title;
        projectEntity.subject = subject;
        projectEntity.priority = parseInt(priority);
        projectEntity.size = parseInt(size);
        projectEntity.description = description;
        projectEntity.required_skills = required_skills;
        await ProjectEntity.save(projectEntity);
        return projectEntity;
    }
    async deleteProject(projectID: number): Promise<ProjectEntity> {
        const project = await ProjectEntity.findOne(projectID);
        await project.remove();
        return project;
    }
}
