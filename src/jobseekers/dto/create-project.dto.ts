import { Length , IsOptional, Min, IsNumber } from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export default class CreateProjectDto {

    @ApiProperty({description:'Title : ', type: "string"})
    readonly title: string;

    @ApiProperty({description:'Subject : ', type: "string",})
    readonly subject: string;

    @ApiProperty({description:"Priority : ", type: "number"})
    readonly priority: string;

    @ApiProperty({description:"Size : ", type: "number"})
    readonly size: string;

    @ApiProperty({description:"Description : ", type: "string"})
    readonly description: string;

    @ApiProperty({description:"Required Skills", type: "string"})
    readonly required_skills: string;
  }