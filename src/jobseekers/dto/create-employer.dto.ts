import { ApiProperty } from '@nestjs/swagger';

export default class CreateEmployerDto {
    @ApiProperty({description:"Firstname : ", type: "string"})
    readonly first_name: string;

    @ApiProperty({description:"Lastname : ", type: "string"})
    readonly last_name: string;

    @ApiProperty({description:"Username : ", type: "string"})
    readonly username: string;

    @ApiProperty({description:"Password", type: "string"})
    readonly password: string;

    @ApiProperty({description:"Email : ", type: "string"})
    readonly email: string;

    @ApiProperty({description:"Password : ", type: "string"})
    readonly phone_number: string;

    @ApiProperty({description:"Address : ", type: "string"})
    readonly address: string;

    @ApiProperty({description:"Projects : ", type: "array", items: {type: "number"}})
    readonly projects: number[] ;
  }