import { Body, Controller, Get, Post, Header, Delete, Put, Query } from '@nestjs/common';
import { BookService } from './book.service';
import CreateBookDto from './dto/create-book.dto';
import {ApiResponse, ApiBearerAuth, ApiQuery } from '@nestjs/swagger';
import UpdateBookDto from './dto/update-book.dto';

@Controller('book')
export default class BookController {
    constructor(private readonly bookServices: BookService) {}
    
    @Header('Content-Type', 'application/json')
    @ApiResponse({ status: 200, description: "" })
    @ApiBearerAuth()
    @Post('addbook')
    postUser( @Body() book: CreateBookDto) {
            return this.bookServices.add(book);
    }

    @ApiResponse({ status: 200, description: "" })
    @ApiBearerAuth()
    @Get()
    getAll() {
        return this.bookServices.getAll();
    }

    @ApiResponse({ status: 200, description: "" })
    @ApiQuery({ name: 'book_id', required: true, type: Number, description: ``})
    @ApiBearerAuth()
    @Delete('delete')
    deleteBook(@Query('book_id') bookID) {
        return this.bookServices.delete(bookID);
    }

    @ApiResponse({ status: 200, description: "" })
    @ApiBearerAuth()
    @Put('update')
    updateBook(@Body() book: UpdateBookDto) {
        return this.bookServices.update(book);
        }
}
