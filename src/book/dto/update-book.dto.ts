import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export default class UpdateBookDto {
  @ApiProperty({description:"ID : ", type: "number"}) 
  readonly id: number;

  @ApiProperty({description:"Name : ", type: "string"}) 
  readonly name: string;

  @ApiProperty({description:"OwenerID", type: "number"}) 
  readonly userID: number;

  @ApiProperty({description:"Genres : ", type: "array", items: {type: "number"}}) 
  readonly genreIDs: number[];
}