import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export default class CreateBookDto {
    @ApiProperty({description:"Name : ", type: "string"})
    readonly name: string;

    @ApiProperty({description:"OwenerID : ", type: "number"})
    readonly userID: number;
    
    @ApiProperty({description:"Genres : ", type: "array", items: {type: "number"}})
    readonly genreIDs: number[];
  }