
import { Module } from '@nestjs/common';
import { BookService } from './book.service';
import BooksController from './book.controller';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { APP_GUARD } from '@nestjs/core';

@Module({
  imports: [],
  controllers: [BooksController],
  providers: [BookService, {
    provide: APP_GUARD,
    useClass: JwtAuthGuard,
  },],
})
export class BookModule {}