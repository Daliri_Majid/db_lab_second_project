import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany } from 'typeorm';
import BookEntity from './book.entity';
@Entity()
export default class UserEntity extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({length: 500, unique:true })
    username: string;
    
    @Column({ length: 500 })
    first_name: string;

    @Column({ length: 500 })
    last_name: string;

    @Column({length: 500})
    password: string;

    @OneToMany( type => BookEntity , book => book.user)
    books: BookEntity[];
}