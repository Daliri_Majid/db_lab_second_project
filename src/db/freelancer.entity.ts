import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany, JoinTable } from 'typeorm';
import ProjectEntity from './project.entity';

@Entity()
export default class FreelancerEntity extends BaseEntity 
{
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column({ length: 500 })
    first_name: string;
  
    @Column({ length: 500 })
    last_name: string;
  
    @Column({length: 500, unique:true })
    username: string;
  
    @Column({length: 500})
    password: string;
  
    @Column({length: 500})
    email: string;
  
    @Column({length: 500})
    phone_number: string;

    @Column({length: 500})
    address: string;
  
    @Column()
    score: number;

    @OneToMany(type => ProjectEntity, project => project.applied_freelancer)
    @JoinTable()
    applied_projects: ProjectEntity[];

    @OneToMany(type => ProjectEntity, project => project.assigned_freelancer)
    @JoinTable()
    accepted_projects: ProjectEntity[];
}