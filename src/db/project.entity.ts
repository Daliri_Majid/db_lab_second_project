import { Entity, PrimaryGeneratedColumn, OneToOne, Column, BaseEntity, ManyToOne} from 'typeorm';
import EmployerEntity from './employer.entity';
import FreelancerEntity from './freelancer.entity';

@Entity()
export default class ProjectEntity extends BaseEntity 
{
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column({ length: 500 })
    title: string;
  
    @Column({ length: 500 })
    subject: string;
  
    @Column()
    priority: number;
  
    @Column()
    size: number;
  
    @Column({length: 2000})
    description: string;
  
    @Column({length: 2000})
    required_skills: string;

    @ManyToOne(type => EmployerEntity, employer => employer.projects)
    employer: EmployerEntity;

    @ManyToOne(type => FreelancerEntity, freelancer => freelancer.applied_projects)
    applied_freelancer: FreelancerEntity;

    @OneToOne(type => FreelancerEntity, freelancer => freelancer.accepted_projects)
    assigned_freelancer: FreelancerEntity;
}