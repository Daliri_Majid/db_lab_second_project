import { Length } from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export default class CreateUserDto {
    @Length(3, 10)
    @ApiProperty({description:'FristName : ', type: "string", minLength: 3, maxLength: 10})
    readonly first_name: string;

    @Length(3, 10)
    @ApiProperty({description:'LastName : ', type: "string", minLength: 3, maxLength: 10})
    readonly last_name: string;

    @ApiProperty({description:"Username : ", type: "string"})
    readonly username: string;

    @ApiProperty({description:"Password : ", type: "string"})
    readonly password: string;

    @ApiProperty({description:"Book Ids : ", type: "array", items: {type: "number"}})
    readonly books: number[] ;
  }