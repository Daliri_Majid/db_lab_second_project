import { Injectable } from '@nestjs/common';
import UserEntity from '../db/user.entity';
import CreateUserDto from './dto/create-user.dto';
import UpdateUserDto from './dto/update-user.dto';
import BookEntity from '../db/book.entity';
import {getConnection} from "typeorm";

@Injectable()
export class UserServices {

  async add(userDetails: CreateUserDto): Promise<UserEntity> {
      const userEntity: UserEntity = UserEntity.create();
      const {first_name, last_name, username, password } = userDetails;
      userEntity.first_name = first_name;
      userEntity.last_name = last_name;
      userEntity.username = username;
      userEntity.password = password;
      await UserEntity.save(userEntity);
      return userEntity;
  }

  async getAll(): Promise<UserEntity[]> {
      return await UserEntity.find();
  }

  async getBooks(userID: number): Promise<BookEntity[]> {
      const user: UserEntity = await UserEntity.findOne({where: {id: userID}, relations: ['books']});
      return user.books;
  }

  async delete(userID: number): Promise<UserEntity> {
      const user = await UserEntity.findOne(userID);
      await user.remove();
      return user;
  }

  async update(userDetails: UpdateUserDto): Promise<UserEntity> {
      const { id, first_name, last_name, username, password } = userDetails;
      const user = await UserEntity.findOne(id);
      if(user != undefined) {
            user.first_name = first_name;
            user.last_name = last_name;
            user.username = username;
            user.password = password;
            await UserEntity.save(user);
      }
      return user;
  }

  async findOneByUsername(username: string): Promise<UserEntity | undefined> {
      return await UserEntity.findOne({where: {username: username}});
  }
}