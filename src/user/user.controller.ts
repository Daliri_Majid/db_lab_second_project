import { Body, Controller, Get, ParseIntPipe, Post, Put, Header, Delete, Query } from '@nestjs/common';
import { UserServices } from './user.service';
import CreateUserDto from './dto/create-user.dto';
import UpdateUserDto from './dto/update-user.dto';
import {ApiResponse, ApiBearerAuth, ApiQuery } from '@nestjs/swagger';

@Controller('users')
export class UserController {
    constructor(private readonly usersServices: UserServices) {}

    @Header('Content-Type', 'application/json')
    @ApiResponse({ status: 200, description: "" })
    @ApiBearerAuth()
    @Post('adduser')
    async postUser( @Body() user: CreateUserDto) {
        return this.usersServices.add(user);
    }

    @ApiResponse({ status: 200, description: "" })
    @ApiBearerAuth()
    @Get()
    getAll() {
        return this.usersServices.getAll();
    }

    @ApiResponse({ status: 200, description: "" })  
    @ApiBearerAuth()
    @Get('book')
    getBooks( @Body('userID', ParseIntPipe) userID: number ) {
        return this.usersServices.getBooks(userID);
    }

    @ApiResponse({ status: 200, description: "" })
    @ApiQuery({name: 'userID', required: true, type: Number, description :``})
    @ApiBearerAuth()
    @Delete('delete')
    deleteUser(@Query('userID') userID) {
        return this.usersServices.delete(userID);
    }

    @ApiResponse({ status: 200, description: "" })
    @ApiBearerAuth()
    @Put('update')
    updateUser(@Body() user: UpdateUserDto) {
        return this.usersServices.update(user);
    }
}